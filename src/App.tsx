import * as React from "react";
import * as ReactDOM from "react-dom/client";
import { createBrowserRouter, RouterProvider } from "react-router-dom";
import LandingScreen from "./Screens/Landing";
import SignUpScreen from "./Screens/Registration/SignUp";
import SignUpTest from "./Screens/Registration/SignUpTest";
import HomeMenu from "./Screens/HomeMenu";
import IATtest_Intro from "./Screens/IATtest/IATtest_Intro";
import IATtest_Instruction from "./Screens/IATtest/IATtest_Instruction";
import IATtest_Main from "./Screens/IATtest/IATtest_Main";
import IATtest_Result from "./Screens/IATtest/IATtest_Result";
import BiasDetector_Input from "./Screens/BiasDetector/BiasDetector_Input";
import BiasDetector_Result from "./Screens/BiasDetector/BiasDetector_Result";
import DiscussionHub from "./Screens/LearningHub/discussionHub";
import AnswerDetail from "./Screens/LearningHub/answerDetail";
import LearningSpace from "./Screens/LearningSpace";

const App = createBrowserRouter([
  {
    path: "/",
    element: <LandingScreen />,
  },
  {
    path: "registration/signUp",
    element: <SignUpScreen />,
  },
  {
    path: "registration/signUpTest",
    element: <SignUpTest />,
  },
  {
    path: "home",
    element: <HomeMenu />,
  },
  {
    path: "testIAT/main",
    element: <IATtest_Main />,
  },
  {
    path: "testIAT/introduction",
    element: <IATtest_Intro />,
  },
  {
    path: "testIAT/instruction",
    element: <IATtest_Instruction />,
  },
  {
    path: "testIAT/result/:testResultParam",
    element: <IATtest_Result />,
  },
  {
    path: "biasDetector/input",
    element: <BiasDetector_Input />,
  },
  {
    path: "biasDetector/result",
    element: <BiasDetector_Result />,
  },
  {
    path: "discussionHub",
    element: <DiscussionHub />,
  },
  {
    path: "discussionHub/answerDetail/:questionParam",
    element: <AnswerDetail />,
  },
  {
    path: "learningSpace",
    element: <LearningSpace />,
  },
]);

export default App;
