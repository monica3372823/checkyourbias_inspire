import { Paper, BottomNavigation, BottomNavigationAction } from "@mui/material";
import {
  HomeOutlined,
  PersonOutline,
  SettingsOutlined,
} from "@mui/icons-material";
import { Link } from "react-router-dom";

function BottomNavBar() {
  return (
    <Paper
      sx={{ position: "fixed", bottom: 0, left: 0, right: 0 }}
      elevation={3}
    >
      <BottomNavigation value={false}>
        <BottomNavigationAction icon={<SettingsOutlined />} />
        <BottomNavigationAction
          icon={<HomeOutlined />}
          component={Link}
          to="/home"
        />
        <BottomNavigationAction icon={<PersonOutline />} />
      </BottomNavigation>
    </Paper>
  );
}

export default BottomNavBar;
