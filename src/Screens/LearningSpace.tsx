import { useState } from "react";
import {
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  FlatList,
  Linking,
} from "react-native";
import { Divider, Box, Stack, Modal, TextField } from "@mui/material";
import { SearchOutlined } from "@mui/icons-material";
import { Link } from "react-router-dom";
import BottomNavBar from "../BottomNav";

interface resource {
  optionText: string;
  optionImage: string;
  optionLink: string;
}

function MaterialOption({ option }: { option: resource }) {
  return (
    <TouchableOpacity
      style={{
        borderRadius: 8,
        borderWidth: 1 / 4,
        height: 180,
        width: 140,
        padding: 12,
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        marginHorizontal: 8,
        //alignItems: "baseline",
      }}
      onPress={() => Linking.openURL(option.optionLink)}
    >
      <img
        src={option.optionImage}
        style={{
          width: "60%",
          height: "60%",
          alignSelf: "center",
          paddingTop: 2,
          paddingBottom: 2,
        }}
      />
      <Text
        style={{ fontWeight: "500", textAlign: "center", marginVertical: 8 }}
      >
        {option.optionText}
      </Text>
    </TouchableOpacity>
  );
}

function LearningSpace() {
  const [learningResource, setLearningResource] = useState<resource[]>([
    {
      optionText: "Implicit Association Test",
      optionImage: "/biasTest.png",
      optionLink: "https://implicit.harvard.edu/implicit/education.html",
    },
    {
      optionText: "How to Reduce Unconscious Biases",
      optionImage: "/landingLogo.png",
      optionLink:
        "https://www.betterworks.com/magazine/unconscious-bias-examples-and-how-to-overcome-them/",
    },
    {
      optionText: "How to Avoid Biases in Writing",
      optionImage: "/learningHub.png",
      optionLink: "https://writingcenter.uagc.edu/avoiding-bias",
    },
    {
      optionText: "Let's Fight Gender Bias in Hong Kong!",
      optionImage: "/gender-equality.png",
      optionLink:
        "https://www.hk-lawyer.org/content/what-can-we-do-move-needle-gender-bias-hong-kong",
    },
  ]);

  return (
    <View style={styles.container}>
      <Box style={styles.container}>
        {/* header and title */}
        <View
          style={{
            display: "flex",
            justifyContent: "center",
            width: "100%",
            paddingHorizontal: 48,
            //top: 0,
            //position: "absolute",
          }}
        >
          <Text style={styles.title}>Learning Space</Text>
          <Divider style={{ marginTop: 8, marginBottom: 8 }} />
          <Text style={{ textAlign: "center", color: "#594f45" }}>
            Find all material that you need to learn more about unconscious
            biases!
          </Text>
        </View>

        {/* search bar */}
        <View
          style={{
            borderWidth: 1 / 4,
            borderRadius: 50,
            padding: 8,
            display: "flex",
            flex: 1,
            flexDirection: "row",
            alignItems: "center",
            width: "full",
            margin: 24,
          }}
        >
          <SearchOutlined />
          <Text style={{ fontSize: 16, fontWeight: "200" }}>
            Search material
          </Text>
        </View>

        <View
          style={{
            display: "flex",
            alignItems: "flex-start",
            paddingHorizontal: 24,
            width: "100vw",
          }}
        >
          <Text style={{ fontSize: 20, fontWeight: "700", color: "#594f45" }}>
            Article of the Day
          </Text>
          <View
            style={{
              borderRadius: 10,
              backgroundColor: "#E0F2FE",
              marginTop: 8,
              width: "100%",
              padding: 12,
            }}
          >
            <Text
              style={{
                fontSize: 16,
                fontWeight: "700",
                marginBottom: 4,
                color: "#594f45",
              }}
            >
              Combating Unconscious Biases in Medicine in 2023
            </Text>
            <Text style={{ fontSize: 12, color: "#594f45" }}>
              Unconscious biases continue to hold back women in medicine, but
              research shows how to fight them and get closer to true equity and
              inclusion
            </Text>
            <TouchableOpacity
              style={{
                backgroundColor: "#132b4f",
                borderRadius: 100,
                padding: 12,
                maxWidth: 120,
                marginTop: 12,
              }}
              onPress={() =>
                Linking.openURL(
                  "https://theconversation.com/unconscious-biases-continue-to-hold-back-women-in-medicine-but-research-shows-how-to-fight-them-and-get-closer-to-true-equity-and-inclusion-200496"
                )
              }
            >
              <Text
                style={{
                  color: "#fdfcfc",
                  fontWeight: "500",
                  textAlign: "center",
                }}
              >
                See full article
              </Text>
            </TouchableOpacity>
          </View>
        </View>

        {/* list of learning resources */}
        <View
          style={{
            flex: 1,
            backgroundColor: "#fdfcfc",
            width: "full",
            marginVertical: 24,
            paddingHorizontal: 24,
          }}
        >
          <Text
            style={{
              fontSize: 20,
              fontWeight: "700",
              color: "#594f45",
              marginBottom: 8,
            }}
          >
            Learning Resources
          </Text>
          <FlatList
            showsHorizontalScrollIndicator={false}
            horizontal
            data={learningResource}
            renderItem={({ item }) => <MaterialOption option={item} />}
          />
        </View>

        <View style={{ alignItems: "center" }}>
          <Text style={{ color: "#594f45" }}>
            Have any suggestion for new materials?
          </Text>
          <TouchableOpacity style={styles.buttonMain}>
            <Text
              style={{
                color: "#fdfcfc",
                fontWeight: "500",
                textAlign: "center",
              }}
            >
              Tell us about your idea!
            </Text>
          </TouchableOpacity>
        </View>

        <BottomNavBar />
      </Box>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column",
    height: "100vh",
    width: "100vw",
    paddingHorizontal: 24,
    paddingVertical: 48,
    alignItems: "center",
    justifyContent: "space-around",
    backgroundColor: "#fdfcfc",
    //position: "relative",
  },
  title: {
    fontSize: 24,
    fontWeight: "bold",
    color: "#594f45",
    textAlign: "center",
    // marginBottom: 8,
  },
  heading: {
    fontSize: 16,
    color: "#594f45",
  },
  buttonMain: {
    backgroundColor: "#132b4f",
    borderRadius: 100,
    padding: 12,
    paddingHorizontal: 36,
    width: "full",
    //position: "absolute",
    marginVertical: 12,
  },
  buttonAlt: {
    backgroundColor: "#fdfcfc",
    borderColor: "#132b4f",
    borderWidth: 1,
    borderRadius: 100,
    padding: 12,
    paddingHorizontal: 36,
  },
});

export default LearningSpace;
