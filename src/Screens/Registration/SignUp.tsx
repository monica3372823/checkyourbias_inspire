import React, { useState } from "react";
import {
  View,
  StyleSheet,
  Text,
  TouchableOpacity,
  TextInput,
} from "react-native";
import {
  Slider,
  Select,
  SelectChangeEvent,
  FormControl,
  MenuItem,
  TextField,
  Divider,
} from "@mui/material";
import { Link } from "react-router-dom";

function FillColumn({ information }: { information: string }) {
  return (
    <View style={styles.filling}>
      <Text style={{ fontWeight: "500", fontSize: 16, marginVertical: 8 }}>
        {information}
      </Text>
      {/* <TextInput
        style={{
          borderRadius: 100,
          borderColor: "#E5E5E5",
          borderWidth: 2,
          marginVertical: 4,
          height: 32,
        }}
      /> */}
      {information === "Name" ? (
        <TextField
          autoComplete="off"
          variant="outlined"
          size="small"
          onChange={(event) =>
            localStorage.setItem("userName", event.target.value)
          }
        />
      ) : (
        <TextField
          autoComplete="off"
          variant="outlined"
          size="small"
          type={information === "Password" ? "password" : "text"}
        />
      )}
    </View>
  );
}

function FillSlider({ information }: { information: string }) {
  const [value, setValue] = useState(0);

  const changeValue = (event: Event, newValue: number | number[]) => {
    setValue(newValue as number);
  };

  return (
    <View style={styles.filling}>
      <Text style={{ fontWeight: "500", fontSize: 16 }}>{information}</Text>
      <Slider value={value} onChange={changeValue} valueLabelDisplay="auto" />
      <Text>{value} years old</Text>
    </View>
  );
}

function FillSelect({
  information,
  choice,
}: {
  information: string;
  choice: string[];
}) {
  const [option, setOption] = useState("See option");

  const changeValue = (event: SelectChangeEvent) => {
    setOption(event?.target?.value as string);
  };

  return (
    <View style={styles.filling}>
      <FormControl fullWidth>
        <Text style={{ fontWeight: "500", fontSize: 16, marginVertical: 8 }}>
          {information}
        </Text>
        <Select
          value={option}
          onChange={changeValue}
          placeholder="See option"
          size="small"
        >
          {choice.map((item, index) => {
            return (
              <MenuItem key={index} value={item}>
                {item}
              </MenuItem>
            );
          })}
        </Select>
      </FormControl>
    </View>
  );
}

function SignUpScreen() {
  // const navigation = useNavigation<NavigationProps>();
  const Genders = ["Female", "Male", "Other"];
  const Ethnicities = [
    "African American",
    "Asian",
    "Hispanic or Latino",
    "Native American or Alaska Native",
    "Pacific Islander",
    "White or Caucasian",
    "Middle Eastern or Arab American",
    "Multiracial or Biracial",
    "Prefer not to answer",
  ];

  return (
    <View style={styles.container}>
      <View style={{ alignItems: "center", width: "100%" }}>
        <Text style={styles.title}>Sign Up</Text>
        <Divider flexItem />
      </View>

      <View style={{ width: "full" }}>
        <FillColumn information="Name" />
        <FillColumn information="Email" />
        <FillColumn information="Location" />
        <FillSlider information="Age" />
        <FillSelect information="Gender" choice={Genders} />
        <FillSelect information="Ethnicity" choice={Ethnicities} />
        <FillColumn information="Password" />
      </View>

      <View>
        <Link to="/testIAT/introduction" style={{ textDecoration: "none" }}>
          <TouchableOpacity
            style={styles.button}
            // onPress={() => navigation.goBack()}
          >
            <Text style={{ color: "#fdfcfc", fontWeight: "500" }}>
              Continue to Sign Up
            </Text>
          </TouchableOpacity>
        </Link>
        <Text style={{ fontWeight: "100" }}>
          Already have account?{" "}
          <Text style={{ fontWeight: "300" }}>Sign In</Text>
        </Text>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column",
    height: "100vh",
    width: "100vw",
    padding: 24,
    alignItems: "center",
    justifyContent: "space-around",
    backgroundColor: "#fdfcfc",
  },
  filling: {
    display: "flex",
    flexDirection: "column",
    paddingVertical: 2,
    flex: 1,
    paddingHorizontal: 24,
    width: "100vw",
  },
  title: {
    fontSize: 24,
    fontWeight: "bold",
    color: "#594f45",
    marginBottom: 8,
  },
  heading: {
    fontSize: 16,
    color: "#594f45",
  },
  button: {
    backgroundColor: "#132b4f",
    borderRadius: 100,
    padding: 12,
    marginVertical: 8,
    alignItems: "center",
  },
});

export default SignUpScreen;
