import { View, Text, TouchableOpacity, StyleSheet } from "react-native";
import { Divider } from "@mui/material";
import { Link } from "react-router-dom";

function SignUpTest() {
  return (
    <View style={styles.container}>
      <View
        style={{
          display: "flex",
          justifyContent: "center",
          width: "100vw",
          paddingHorizontal: 48,
          // alignItems: "center",
        }}
      >
        <Text style={styles.title}>Sign Up</Text>
        <Divider style={{ marginTop: 8 }} />
      </View>

      <Text
        style={{
          alignItems: "center",
          lineHeight: 24,
          fontSize: 16,
          textAlign: "center",
        }}
      >
        You will take four tests that can help you to discover potential biases
        that you have. It will take approximately 10 minutes in total.
      </Text>

      <View
        style={{
          display: "flex",
          flexDirection: "row",
          justifyContent: "space-evenly",
          width: "100vw",
          padding: 24,
        }}
      >
        <Link to="/testIAT/main" style={{ textDecoration: "none" }}>
          <TouchableOpacity
            style={styles.buttonMain}
            // onPress={() => navigation.goBack()}
          >
            <Text style={{ color: "#fdfcfc", fontWeight: "500" }}>
              Take test now{" "}
            </Text>
          </TouchableOpacity>
        </Link>

        <Link to="/home" style={{ textDecoration: "none" }}>
          <TouchableOpacity
            style={styles.buttonAlt}
            // onPress={() => navigation.goBack()}
          >
            <Text style={{ color: "#132b4f", fontWeight: "300" }}>
              Take test later{" "}
            </Text>
          </TouchableOpacity>
        </Link>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column",
    height: "100vh",
    width: "100vw",
    padding: 24,
    alignItems: "center",
    justifyContent: "space-around",
    backgroundColor: "#fdfcfc",
  },
  title: {
    fontSize: 24,
    fontWeight: "bold",
    color: "#594f45",
    textAlign: "center",
    // marginBottom: 8,
  },
  heading: {
    fontSize: 16,
    color: "#594f45",
  },
  buttonMain: {
    backgroundColor: "#132b4f",
    borderRadius: 100,
    padding: 12,
  },
  buttonAlt: {
    backgroundColor: "#fdfcfc",
    borderColor: "#132b4f",
    borderWidth: 1,
    borderRadius: 100,
    padding: 12,
  },
});

export default SignUpTest;
