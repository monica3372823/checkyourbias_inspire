import { useState } from "react";
import {
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  FlatList,
} from "react-native";
import { Divider, Box, Stack, Modal, TextField } from "@mui/material";
import { SearchOutlined } from "@mui/icons-material";
import { Link } from "react-router-dom";
import BottomNavBar from "../../BottomNav";

interface answerDetail {
  id: string;
  questionID: string;
  sender: string;
  answer: string;
  time: string;
}

interface questionDetail {
  id: string;
  question: string;
  sender: string;
  numAnswer: number;
  time: string;
  answers?: answerDetail[];
}

function QuestionCard({ questionData }: { questionData: questionDetail }) {
  const stringQuestion = encodeURIComponent(JSON.stringify(questionData));

  return (
    <Link
      to={`/discussionHub/answerDetail/${stringQuestion}`}
      style={{ textDecoration: "none" }}
    >
      <TouchableOpacity
        style={{
          display: "flex",
          //alignItems: "flex-start",
          paddingVertical: 12,
          paddingHorizontal: 12,
          width: "full",
          borderRadius: 8,
          borderWidth: 1 / 4,
          borderColor: "#594f45",
          marginBottom: 12,
        }}
      >
        <Stack spacing={1}>
          <Text style={{ fontSize: 14, fontWeight: "400" }}>
            {questionData.time}
          </Text>
          <Text style={{ fontSize: 16, fontWeight: "500" }}>
            {questionData.question}
          </Text>
          <View
            style={{
              display: "flex",
              flexDirection: "row",
              width: "full",
              justifyContent: "space-between",
              alignItems: "center",
            }}
          >
            <Text style={{ fontSize: 12, fontWeight: "300" }}>
              {questionData.sender}
            </Text>
            {questionData.numAnswer ? (
              <Text style={{ fontSize: 12, fontWeight: "300" }}>
                {questionData.numAnswer} Answer(s)
              </Text>
            ) : (
              <Text style={{ fontSize: 12, fontWeight: "300" }}>No Answer</Text>
            )}
          </View>
        </Stack>
      </TouchableOpacity>
    </Link>
  );
}

function QuestionOfTheDay({ questionData }: { questionData: questionDetail }) {
  const stringQuestion = encodeURIComponent(JSON.stringify(questionData));

  return (
    <Link
      to={`/discussionHub/answerDetail/${stringQuestion}`}
      style={{ textDecoration: "none" }}
    >
      <View
        style={{
          display: "flex",
          alignItems: "flex-start",
          //paddingHorizontal: 24,
          width: "full",
        }}
      >
        <Text style={{ fontSize: 20, fontWeight: "700", color: "#594f45" }}>
          Question of the Day
        </Text>
        <View
          style={{
            borderRadius: 10,
            borderWidth: 1 / 4,
            backgroundColor: "#E0F2FE",
            marginTop: 12,
            marginBottom: 24,
            width: "100%",
            paddingHorizontal: 12,
            paddingVertical: 12,
            //height: 160,
          }}
        >
          <Text style={{ fontSize: 16, fontWeight: "500" }}>
            {questionData.question}
          </Text>
        </View>
      </View>
    </Link>
  );
}

function DiscussionHub() {
  const [questionList, setQuestionList] = useState<questionDetail[]>([
    {
      id: "1",
      question: "How can I promote the awareness of gender biases? #equality",
      sender: "QOTD",
      numAnswer: 3,
      time: "6 hours ago",
    },
    {
      id: "2",
      question: "How to be more aware with unconscious biases in university?",
      sender: "Anonymous",
      numAnswer: 0,
      time: "1 day ago",
    },
    {
      id: "3",
      question: "I want to make a fair review to my employees. Any tips?",
      sender: "TaySwift13",
      numAnswer: 3,
      time: "3 days ago",
    },
    {
      id: "4",
      question:
        "I just saw someone being racist to my friend. What should I do?",
      sender: "Anonymous",
      numAnswer: 2,
      time: "6 days ago",
    },
    {
      id: "5",
      question: "How to stop ageism bias in your surrounding?",
      sender: "RyanRey2",
      numAnswer: 1,
      time: "2 weeks ago",
    },
    {
      id: "6",
      question: "Anyone up for discussion and coffee this Saturday?",
      sender: "Jenn_Cheung",
      numAnswer: 11,
      time: "1 month ago",
    },
  ]);

  const [newQuestion, setNewQuestion] = useState("");

  const [openModal, setOpenModal] = useState(false);

  function addQuestion(newQuest: string) {
    const newQuestionData: questionDetail = {
      id: (questionList.length + 1).toString(),
      question: newQuest,
      sender: "Anonymous",
      numAnswer: 0,
      time: "Just now",
    };

    setQuestionList([newQuestionData, ...questionList]);
  }

  function NewQuestionModal() {
    return (
      <Box sx={styles.modal}>
        <Text
          style={{
            fontSize: 16,
            fontWeight: "700",
            color: "#594f45",
          }}
        >
          This is a safe space, ask around!
        </Text>

        <View
          style={{
            borderRadius: 10,
            backgroundColor: "#E0F2FE",
            marginVertical: 12,
            width: "100%",
            padding: 12,
            minHeight: 120,
            //height: 160,
          }}
        >
          <TextField
            multiline
            variant="standard"
            InputProps={{ disableUnderline: true }}
            placeholder="Type your question here"
            value={newQuestion}
            onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
              setNewQuestion(event.target.value);
            }}
            style={{
              fontSize: 16,
              fontWeight: "200",
            }}
          />
        </View>

        <View>
          <TouchableOpacity
            style={{
              backgroundColor: "#132b4f",
              borderRadius: 100,
              padding: 12,
              alignSelf: "flex-start",
            }}
            onPress={() => addQuestion(newQuestion)}
          >
            <Text style={{ color: "#fdfcfc", textAlign: "center" }}>
              Add Question
            </Text>
          </TouchableOpacity>
        </View>
      </Box>
    );
  }

  return (
    <View style={styles.container}>
      <Box>
        {/* header and title */}
        <View
          style={{
            //display: "flex",
            justifyContent: "center",
            width: "100%",
            //paddingHorizontal: 48,
            //top: 0,
            //position: "absolute",
          }}
        >
          <Text style={styles.title}>Discussion Hub</Text>
          <Divider style={{ marginTop: 8, marginBottom: 8 }} />
          <Text style={{ textAlign: "center" }}>
            Discuss about unconscious biases with your peers
          </Text>
        </View>

        {/* search bar */}
        <View
          style={{
            //flex: 3,
            //flexDirection: "column",
            //height: "100vh",
            width: "full",
            paddingVertical: 24,
            alignItems: "center",
            justifyContent: "space-around",
            backgroundColor: "#fdfcfc",
            //top: 0,
            //position: "absolute",
          }}
        >
          <View
            style={{
              borderWidth: 1 / 4,
              borderRadius: 50,
              padding: 8,
              //paddingVertical: 16,
              display: "flex",
              flex: 1,
              flexDirection: "row",
              alignItems: "center",
              width: "100%",
              //marginVertical: 16,
            }}
          >
            <SearchOutlined />
            <Text style={{ fontSize: 16, fontWeight: "200" }}>Search here</Text>
          </View>
        </View>

        <QuestionOfTheDay
          questionData={
            questionList.find((question) => {
              return question.sender === "QOTD";
            })!
          }
        />

        {/* list of questions and QOTD */}
        <FlatList
          data={questionList.filter((question) => {
            return question.sender !== "QOTD";
          })}
          renderItem={({ item }) => <QuestionCard questionData={item} />}
          keyExtractor={(item) => item.id}
          // ListHeaderComponent={
          //   <QuestionOfTheDay
          //     questionData={
          //       questionList.find((question) => {
          //         return question.sender === "QOTD";
          //       })!
          //     }
          //   />
          // }
          style={{
            flex: 1,
            height: 360,
            backgroundColor: "#fdfcfc",
            //position: "absolute",
          }}
        />

        <View style={{ alignItems: "center", marginVertical: 24 }}>
          <TouchableOpacity
            style={styles.buttonMain}
            onPress={() => setOpenModal(true)}
          >
            <Text style={{ color: "#fdfcfc" }}>Write New Question</Text>
          </TouchableOpacity>
        </View>

        <Modal open={openModal} onClose={() => setOpenModal(false)}>
          <NewQuestionModal />
        </Modal>

        {/* </View> */}
      </Box>

      <BottomNavBar />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    //flexDirection: "column",
    //height: "100vh",
    //width: "100vw",
    paddingHorizontal: 24,
    paddingVertical: 48,
    alignItems: "center",
    justifyContent: "space-around",
    backgroundColor: "#fdfcfc",
    //position: "relative",
  },
  title: {
    fontSize: 24,
    fontWeight: "bold",
    color: "#594f45",
    textAlign: "center",
    // marginBottom: 8,
  },
  heading: {
    fontSize: 16,
    color: "#594f45",
  },
  buttonMain: {
    backgroundColor: "#132b4f",
    borderRadius: 100,
    padding: 12,
    paddingHorizontal: 36,
    width: "full",
    //position: "absolute",
    //marginBottom: 24,
  },
  buttonAlt: {
    backgroundColor: "#fdfcfc",
    borderColor: "#132b4f",
    borderWidth: 1,
    borderRadius: 100,
    padding: 12,
    paddingHorizontal: 36,
  },
  modal: {
    position: "absolute" as "absolute",
    top: "25%",
    left: "10%",
    right: "10%",
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",
    width: "full",
    backgroundColor: "#fdfcfc",
    boxShadow: 24,
    p: 2,
    borderRadius: 2,
  },
});

export default DiscussionHub;
export { QuestionCard };
export type { answerDetail, questionDetail };
