import { useState, useEffect } from "react";
import {
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  FlatList,
  TextInput,
} from "react-native";
import {
  Divider,
  Box,
  Stack,
  TextField,
  IconButton,
  Button,
} from "@mui/material";
import {
  SearchOutlined,
  SendOutlined,
  ReplyOutlined,
} from "@mui/icons-material";
import { Link, useParams } from "react-router-dom";
import { QuestionCard, answerDetail, questionDetail } from "./discussionHub";
import { stringify } from "querystring";

function AnswerCard({ answerData }: { answerData: answerDetail }) {
  return (
    <View
      style={{
        display: "flex",
        paddingVertical: 8,
        width: "full",
      }}
    >
      <Stack spacing={1}>
        <View
          style={{
            display: "flex",
            flexDirection: "row",
            width: "full",
            justifyContent: "space-between",
            alignItems: "center",
          }}
        >
          <Text style={{ fontSize: 16, fontWeight: "500" }}>
            {answerData.answer}
          </Text>
          <ReplyOutlined />
        </View>

        <View
          style={{
            display: "flex",
            flexDirection: "row",
            width: "full",
            justifyContent: "space-between",
            alignItems: "center",
          }}
        >
          <Text style={{ fontSize: 12, fontWeight: "300" }}>
            {answerData.sender}
          </Text>
          <Text style={{ fontSize: 12, fontWeight: "300" }}>
            {answerData.time}
          </Text>
        </View>

        <Divider flexItem />
      </Stack>
    </View>
  );
}

function AnswerDetail() {
  const [answersList, setAnswerList] = useState<answerDetail[]>([
    {
      id: "1",
      questionID: "1",
      sender: "Anonymous",
      answer: "I think you can start by using more gender neutral pronoun!",
      time: "1 hour ago",
    },
    {
      id: "2",
      questionID: "1",
      sender: "Barbie_XO",
      answer:
        "Share stories to your surrounding! Sometime, people just don't notice.",
      time: "3 hours ago",
    },
    {
      id: "3",
      questionID: "1",
      sender: "TimotheeCh4l",
      answer: "I'm not really sure either, anyone have idea?",
      time: "5 hours ago",
    },
    {
      id: "3",
      questionID: "5",
      sender: "Anonymous",
      answer:
        "Try to form a group with people cross-age! I'm sure everyone can learn from different experiences :)",
      time: "2 weeks ago",
    },
  ]);

  const [questionInformation, setQuestionInformation] =
    useState<questionDetail>({
      id: "",
      question: "",
      sender: "",
      numAnswer: 0,
      time: "",
    });

  const [replyText, setReplyText] = useState("");

  const { questionParam } = useParams();

  function addAnswer(newAnswer: string) {
    const newAnswerData: answerDetail = {
      id: (answersList.length + 1).toString(),
      questionID: questionInformation.id,
      sender: "Anonymous",
      answer: newAnswer,
      time: "Just now",
    };

    setAnswerList([newAnswerData, ...answersList]);
  }

  useEffect(() => {
    if (questionParam) {
      const decodedString = decodeURIComponent(questionParam);
      const questionObject = JSON.parse(decodedString);
      setQuestionInformation(questionObject);
    }
  }, [questionParam]);

  return (
    <View>
      {questionParam ? (
        <Box
          sx={{
            position: "relative",
          }}
        >
          <View style={styles.container}>
            <Box>
              {/* header and title */}
              <View
                style={{
                  justifyContent: "center",
                  width: "100%",
                  marginBottom: 24,
                }}
              >
                <Text style={styles.title}>Discussion Hub</Text>
                <Divider style={{ marginTop: 8, marginBottom: 8 }} />
              </View>

              <QuestionCard questionData={questionInformation} />

              {/* list of answers */}
              <View
                style={{
                  justifyContent: "center",
                  width: "100%",
                  marginVertical: 24,
                }}
              >
                <Text
                  style={{
                    fontSize: 20,
                    fontWeight: "700",
                    color: "#594f45",
                    marginBottom: 6,
                  }}
                >
                  Answers
                </Text>
                <FlatList
                  data={answersList.filter((answer) => {
                    return answer.questionID === questionInformation.id;
                  })}
                  renderItem={({ item }) => <AnswerCard answerData={item} />}
                  keyExtractor={(item) => item.id}
                  style={{
                    flex: 1,
                    height: 400,
                    backgroundColor: "#fdfcfc",
                    //position: "absolute",
                  }}
                />
              </View>
            </Box>
          </View>

          <View style={{ bottom: 0, position: "absolute", width: "100%" }}>
            <Divider flexItem />
            <View
              style={{
                width: "100%",
                paddingVertical: 24,
                paddingHorizontal: 32,
                alignItems: "center",
                justifyContent: "space-around",
                backgroundColor: "#fdfcfc",
                display: "flex",
                flexDirection: "row",
              }}
            >
              <View
                style={{
                  borderWidth: 1 / 4,
                  borderRadius: 50,
                  paddingVertical: 8,
                  paddingHorizontal: 16,
                  width: "100%",
                  marginRight: 8,
                }}
              >
                {/* <Text style={{ fontSize: 16, fontWeight: "200" }}>Search here</Text> */}
                <TextField
                  variant="standard"
                  InputProps={{ disableUnderline: true }}
                  placeholder="Type your answer here"
                  value={replyText}
                  onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
                    setReplyText(event.target.value);
                  }}
                  style={{
                    fontSize: 16,
                    fontWeight: "200",
                  }}
                />
              </View>
              <IconButton
                onClick={() => {
                  addAnswer(replyText);
                  setReplyText("");
                }}
              >
                <SendOutlined />
              </IconButton>
            </View>
          </View>
        </Box>
      ) : (
        <View style={styles.container}>
          <Text>Loading.....</Text>
        </View>
      )}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    //flexDirection: "column",
    top: 0,
    height: "100vh",
    width: "100vw",
    paddingHorizontal: 24,
    paddingVertical: 48,
    alignItems: "center",
    justifyContent: "space-between",
    backgroundColor: "#fdfcfc",
  },
  title: {
    fontSize: 24,
    fontWeight: "bold",
    color: "#594f45",
    textAlign: "center",
    // marginBottom: 8,
  },
  heading: {
    fontSize: 16,
    color: "#594f45",
  },
  buttonMain: {
    backgroundColor: "#132b4f",
    borderRadius: 100,
    padding: 12,
    paddingHorizontal: 36,
    position: "absolute",
    //marginBottom: 24,
  },
  buttonAlt: {
    backgroundColor: "#fdfcfc",
    borderColor: "#132b4f",
    borderWidth: 1,
    borderRadius: 100,
    padding: 12,
    paddingHorizontal: 36,
  },
});

export default AnswerDetail;
