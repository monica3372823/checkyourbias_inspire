import { url } from "inspector";
import React from "react";
import { View, StyleSheet, Text, TouchableOpacity, Image } from "react-native";
import { Link } from "react-router-dom";

function LandingScreen() {
  return (
    <View style={styles.container}>
      <View
        style={{
          alignItems: "center",
          marginBottom: 12,
        }}
      >
        <Text style={styles.title}>CheckYourBias</Text>
        <Text style={styles.heading}>
          Your solution to tackle unconscious biases
        </Text>
      </View>

      <img src="/landingLogo.png" alt="Logo for landing poage" />
      <Link to="/registration/signUp" style={{ textDecoration: "none" }}>
        <TouchableOpacity
          style={styles.button}
          // onPress={() => navigation.navigate("SignUp")}
        >
          <Text style={{ color: "#fdfcfc", fontWeight: "500" }}>
            Let's get started!
          </Text>
        </TouchableOpacity>
      </Link>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column",
    height: "100vh",
    width: "100vw",
    padding: 24,
    alignItems: "center",
    justifyContent: "space-evenly",
    backgroundColor: "#fdfcfc",
  },
  title: {
    fontSize: 28,
    fontWeight: "bold",
    color: "#594f45",
  },
  heading: {
    fontSize: 18,
    color: "#594f45",
    marginTop: 4,
  },
  button: {
    backgroundColor: "#132b4f",
    borderRadius: 100,
    padding: 12,
  },
});

export default LandingScreen;
