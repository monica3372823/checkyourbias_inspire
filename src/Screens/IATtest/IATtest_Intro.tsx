import { View, Text, TouchableOpacity, StyleSheet } from "react-native";
import { Divider, Stack } from "@mui/material";
import { Link } from "react-router-dom";
import BottomNavBar from "../../BottomNav";

function IATtest_Intro() {
  return (
    <View style={styles.container}>
      <View
        style={{
          display: "flex",
          justifyContent: "center",
          width: "100vw",
          paddingHorizontal: 48,
        }}
      >
        <Text style={styles.title}>Potential Biases Test</Text>
        <Divider style={{ marginTop: 8, marginBottom: 8 }} />
        <Text style={{ textAlign: "center" }}>
          Inspired by Implicit Association Test
        </Text>
      </View>

      <Text
        style={{
          alignItems: "center",
          lineHeight: 24,
          fontSize: 16,
          textAlign: "center",
          color: "#594f45",
        }}
      >
        You will take four tests that can help you to discover potential biases
        that you have. These tests are
        <Text style={{ fontWeight: "700" }}>
          {" "}
          Gender-Career Bias Test, Skin Tone Bias Test, Age Bias Test, and
          Sexuality Bias Test.
        </Text>
        {"\n"}
        {"\n"}Each test consist of four parts and you can take a break in
        between. It will take approximately 10 minutes in total to finish all
        the tests.
      </Text>

      <Stack spacing={1}>
        <View
          style={{
            display: "flex",
            flexDirection: "row",
            justifyContent: "space-around",
            width: "100vw",
            paddingHorizontal: 24,
          }}
        >
          <Link to="/testIAT/instruction" style={{ textDecoration: "none" }}>
            <TouchableOpacity
              style={styles.buttonMain}
              // onPress={() => navigation.goBack()}
            >
              <Text
                style={{
                  color: "#fdfcfc",
                  fontWeight: "500",
                  textAlign: "center",
                }}
              >
                Take test now{" "}
              </Text>
            </TouchableOpacity>
          </Link>

          <Link to="/home" style={{ textDecoration: "none" }}>
            <TouchableOpacity
              style={styles.buttonAlt}
              // onPress={() => navigation.goBack()}
            >
              <Text
                style={{
                  color: "#132b4f",
                  fontWeight: "300",
                  textAlign: "center",
                }}
              >
                Take test later{" "}
              </Text>
            </TouchableOpacity>
          </Link>
        </View>

        <View
          style={{
            display: "flex",
            flexDirection: "column",
            flex: 1,
            paddingHorizontal: 48,
          }}
        >
          <Text
            style={{
              fontSize: 16,
              color: "#594f45",
              textAlign: "center",
              marginBottom: 12,
            }}
          >
            or
          </Text>

          <Link to="/testIAT/record" style={{ textDecoration: "none" }}>
            <TouchableOpacity
              style={styles.buttonAlt}
              // onPress={() => navigation.goBack()}
            >
              <Text
                style={{
                  fontWeight: "500",
                  textAlign: "center",
                }}
              >
                See Record
              </Text>
            </TouchableOpacity>
          </Link>
        </View>
      </Stack>

      <BottomNavBar />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column",
    height: "100vh",
    width: "100vw",
    padding: 24,
    alignItems: "center",
    justifyContent: "space-around",
    backgroundColor: "#fdfcfc",
  },
  title: {
    fontSize: 24,
    fontWeight: "bold",
    color: "#594f45",
    textAlign: "center",
    // marginBottom: 8,
  },
  heading: {
    fontSize: 16,
    color: "#594f45",
  },
  buttonMain: {
    backgroundColor: "#132b4f",
    borderRadius: 100,
    padding: 12,
    minWidth: 140,
  },
  buttonAlt: {
    backgroundColor: "#fdfcfc",
    borderColor: "#132b4f",
    borderWidth: 1,
    borderRadius: 100,
    padding: 12,
    minWidth: 140,
  },
});

export default IATtest_Intro;
