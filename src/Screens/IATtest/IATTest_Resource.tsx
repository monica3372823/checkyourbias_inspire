import React from "react";

interface objectDetail {
  [key: string]: {
    name: string;
    stimulusMedia: { word: string }[];
  };
}

interface wordPair {
  name: string;
  word: string;
}

type PropertyNames<T> = {
  [K in keyof T]: T[K] extends object ? K : never;
}[keyof T];
type CategoryNames<T> = Extract<PropertyNames<T>, "category1" | "category2">;
type AttributeNames<T> = Extract<PropertyNames<T>, "attribute1" | "attribute2">;
type BothNames<T> = Extract<
  PropertyNames<T>,
  "category1" | "category2" | "attribute1" | "attribute2"
>;

const resourceGenderCareerBias = {
  category1: {
    name: "Career",
    stimulusMedia: [
      { word: "Career" },
      { word: "Corporation" },
      { word: "Salary" },
      { word: "Office" },
      { word: "Professional" },
      { word: "Management" },
      { word: "Business" },
    ],
  },
  category2: {
    name: "Family",
    stimulusMedia: [
      { word: "Wedding" },
      { word: "Marriage" },
      { word: "Parents" },
      { word: "Relatives" },
      { word: "Family" },
      { word: "Home" },
      { word: "Children" },
    ],
  },
  attribute1: {
    name: "Male",
    stimulusMedia: [
      { word: "Ben" },
      { word: "Paul" },
      { word: "Daniel" },
      { word: "John" },
      { word: "Jeffrey" },
    ],
  },
  attribute2: {
    name: "Female",
    stimulusMedia: [
      { word: "Rebecca" },
      { word: "Michelle" },
      { word: "Emily" },
      { word: "Julia" },
      { word: "Anna" },
    ],
  },
};

function createWordsArray<T extends objectDetail>(
  obj: T,
  ...properties: (CategoryNames<T> | AttributeNames<T> | BothNames<T>)[]
) {
  const validProperties = Object.keys(obj).filter(
    (key): key is CategoryNames<T> | AttributeNames<T> | BothNames<T> => {
      return (
        obj[key].stimulusMedia &&
        properties.includes(
          key as CategoryNames<T> | AttributeNames<T> | BothNames<T>
        )
      );
    }
  );

  return validProperties.flatMap((property) => {
    const category = obj[property];
    return category.stimulusMedia.map((media) => {
      return {
        name: category.name,
        word: media.word,
      };
    });
  });
}

const arrayFirstGC = createWordsArray(
  resourceGenderCareerBias,
  "category1",
  "category2"
); // categorize career and family attribute
const arraySecondGC = createWordsArray(
  resourceGenderCareerBias,
  "attribute1",
  "attribute2"
); // categorize female & male name
const arrayRestGC = createWordsArray(
  resourceGenderCareerBias,
  "category1",
  "category2",
  "attribute1",
  "attribute2"
);
// categorize male-career, female-family (3rd) and male-family, female-career (4th)

function randomizeArray(testArray: wordPair[]) {
  const newArray = [...testArray];

  for (let i = newArray.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + 1));
    [newArray[i], newArray[j]] = [newArray[j], newArray[i]];
  }
  return newArray;
}

export { randomizeArray, arrayFirstGC, arraySecondGC, arrayRestGC };
export type { wordPair };
