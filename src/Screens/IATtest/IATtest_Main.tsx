import { View, Text, TouchableOpacity, StyleSheet } from "react-native";
import { Avatar, Divider, LinearProgress, Stack } from "@mui/material";
import React, { useState, useRef, useEffect } from "react";
import {
  randomizeArray,
  arrayFirstGC,
  arraySecondGC,
  arrayRestGC,
  wordPair,
} from "./IATTest_Resource";
import { Link } from "react-router-dom";
import BottomNavBar from "../../BottomNav";

function IATtest_Main() {
  const [currentWordIndex, setCurrentWordIndex] = useState(0);
  const [firstOption, setFirstOption] = useState("");
  const [secondOption, setSecondOption] = useState("");
  //const [answer, setAnswer] = useState("");
  const [score, setScore] = useState(0);
  const [orderTest, setOrderTest] = useState(1);
  const [startTimer, setStartTimer] = useState(false);
  const [answerWrong, setAnswerWrong] = useState(false);
  const [wordArray, setWordArray] = useState<wordPair[]>();
  const [mistakeCount, setMistakeCount] = useState(0);

  const [startTimePart3, setStartTimePart3] = useState(null);
  const [finishTimePart3, setFinishTimePart3] = useState(null);
  const [startTimePart4, setStartTimePart4] = useState(null);
  const [finishTimePart4, setFinishTimePart4] = useState(null);
  const [testResult, setTestResult] = useState({
    totalTime3: 0,
    totalTime4: 0,
    totalMistakes: 0,
  });

  //const [showAnswer, setShowAnswer] = useState(false);
  const timerRef = useRef<NodeJS.Timeout | null>(null);

  useEffect(() => {
    setOrderTest(1);
    setWordArray(randomizeArray(arrayFirstGC));
    setScore(0);
    setCurrentWordIndex(0);
    setFirstOption("Career");
    setSecondOption("Family");
  }, []);

  useEffect(() => {
    if (orderTest === 1) {
      setFirstOption("Career");
      setSecondOption("Family");
      setWordArray(randomizeArray(arrayFirstGC));
      console.log("next");
    } else if (orderTest === 2) {
      setFirstOption("Male");
      setSecondOption("Female");
      setWordArray(randomizeArray(arraySecondGC));
      console.log("next");
    } else if (orderTest === 3) {
      setFirstOption("Male or Career");
      setSecondOption("Female or Family");
      setWordArray(randomizeArray(arrayRestGC));
      //setStartTimer(true);

      const startTime = new Date();
      if (startTime === null) {
        setStartTimePart3(startTime);
      }
      console.log("next");
    } else {
      const endTime = new Date();
      if (endTime === null) {
        setFinishTimePart3(endTime);
      }

      setFirstOption("Female or Career");
      setSecondOption("Male or Family");
      setWordArray(randomizeArray(arrayRestGC));
      //setStartTimer(true);

      const startTime = new Date();
      if (startTime === null) {
        setStartTimePart4(startTime);
      }

      console.log("next");
    }

    setCurrentWordIndex(0);
    console.log("index");
  }, [orderTest]);

  useEffect(() => {
    if (
      orderTest === 4 &&
      wordArray &&
      currentWordIndex === wordArray.length - 1
    ) {
      const endTime = new Date();
      if (endTime === null) {
        setFinishTimePart4(endTime);
      }
    }

    let totalTimeTest3 = 0;
    let totalTimeTest4 = 0;
    if (
      finishTimePart3 &&
      startTimePart3 &&
      finishTimePart4 &&
      startTimePart4
    ) {
      totalTimeTest3 = finishTimePart3 - startTimePart3 / 1000;
      totalTimeTest4 = finishTimePart4 - startTimePart4 / 1000;
    }

    setTestResult({
      totalTime3: totalTimeTest3,
      totalTime4: totalTimeTest4,
      totalMistakes: mistakeCount,
    });
  }, [currentWordIndex]);

  // useEffect(() => {
  //   // Start the timer when the current word changes AND go to 3rd & 4th round
  //   if (startTimer) {
  //     const timer = setTimeout(() => {
  //       setScore(score - 0.5);
  //       console.log("time out");
  //     }, 4000);
  //     timerRef.current = timer;

  //     return () => {
  //       // Stop the timer when the current word changes or the component unmounts
  //       clearTimeout(timerRef.current!);
  //     };
  //   }
  // }, [startTimer, currentWordIndex]);

  function handleSubmit(answer: string) {
    if (wordArray) {
      // if user goes to 3rd&4th test (association bias part), start updating score
      let isAnswerTrue = false;

      if (orderTest > 2) {
        const possibleAnswer = answer.split(" or ");
        isAnswerTrue = possibleAnswer.includes(
          wordArray[currentWordIndex].name
        );
      } else {
        isAnswerTrue = answer == wordArray[currentWordIndex].name;
      }

      // if (isAnswerTrue) {
      //   setAnswerWrong(false);
      //   setCurrentWordIndex(currentWordIndex + 1);
      // } else {
      //   if (orderTest > 2) {
      //     setScore(score - 1);
      //     setMistakeCount(mistakeCount + 1);
      //   }
      //   setAnswerWrong(true);
      // }
      if (!isAnswerTrue && orderTest > 2) {
        setScore(score - 1);
        setMistakeCount(mistakeCount + 1);
      }

      setCurrentWordIndex(currentWordIndex + 1);
    }
  }

  function continueNextTest() {
    setOrderTest(orderTest + 1);
    // if (orderTest === 4) {
    //   console.log(score);
    //   setTestFinished(true);
    // } else {
    //   setOrderTest(orderTest + 1);
    // }
  }

  return (
    <View style={styles.container}>
      <View>
        {/* header & title */}
        <View
          style={{
            display: "flex",
            justifyContent: "center",
            width: "100vw",
            paddingHorizontal: 48,
          }}
        >
          <Text style={styles.title}>Gender-Career Bias Test</Text>
          <Divider style={{ marginTop: 8, marginBottom: 8 }} />
          <Text style={{ textAlign: "center" }}>
            Potential Biases Test inspired by Implicit Association Test
          </Text>
        </View>

        {/* test progress */}
        <View
          style={{
            display: "flex",
            justifyContent: "center",
            width: "100vw",
            paddingHorizontal: 48,
            marginVertical: 48,
          }}
        >
          <LinearProgress
            style={{
              marginTop: 8,
              marginBottom: 8,
              width: "80%",
              alignSelf: "center",
            }}
            variant="determinate"
            value={orderTest * 25}
          />
          <Text
            style={{
              textAlign: "center",
              fontWeight: "500",
              fontSize: 20,
              color: "#594f45",
            }}
          >
            Part {orderTest}
          </Text>
        </View>
      </View>

      {wordArray && currentWordIndex < wordArray.length ? (
        <View
          style={{
            paddingVertical: 24,
            display: "flex",
            flex: 1,
            justifyContent: "space-around",
          }}
        >
          <Text
            style={{
              fontSize: 28,
              fontWeight: "bold",
              color: "#594f45",
              textAlign: "center",
            }}
          >
            {wordArray[currentWordIndex].word}
          </Text>

          <View>
            <Stack spacing={1}>
              {/* button to choose options */}
              <View
                style={{
                  display: "flex",
                  flexDirection: "row",
                  justifyContent: "space-around",
                  width: "100vw",
                  paddingHorizontal: 24,
                  paddingVertical: 12,
                }}
              >
                <TouchableOpacity
                  style={styles.buttonMain}
                  onPress={() => handleSubmit(firstOption)}
                >
                  <Text
                    style={{
                      color: "#fdfcfc",
                      fontWeight: "500",
                      fontSize: 16,
                      textAlign: "center",
                    }}
                  >
                    {firstOption}
                  </Text>
                </TouchableOpacity>

                <TouchableOpacity
                  style={styles.buttonAlt}
                  onPress={() => handleSubmit(secondOption)}
                >
                  <Text
                    style={{
                      color: "#132b4f",
                      fontWeight: "300",
                      fontSize: 16,
                      textAlign: "center",
                    }}
                  >
                    {secondOption}
                  </Text>
                </TouchableOpacity>
              </View>
              {/* progress of current part */}
              <Text
                style={{
                  fontWeight: "500",
                  fontSize: 24,
                  textAlign: "center",
                  color: "#594f45",
                }}
              >
                {currentWordIndex + 1}/{wordArray.length}
              </Text>

              {/* <Text>{score}</Text> */}

              {/* {answerWrong && (
                <Stack spacing={1}>
                  <Text
                    style={{
                      color: "#E11D48",
                      fontSize: 20,
                      fontWeight: "bold",
                      textAlign: "center",
                    }}
                  >
                    Wrong answer!
                  </Text>

                  {orderTest > 2 && (
                    <Text
                      style={{
                        color: "#594f45",
                        fontSize: 16,
                        fontWeight: "400",
                        textAlign: "center",
                      }}
                    >
                      Current mistake(s) : {mistakeCount}
                    </Text>
                  )}
                </Stack>
              )} */}
            </Stack>
          </View>
        </View>
      ) : (
        <View
          style={{
            paddingVertical: 24,
            display: "flex",
            flex: 1,
            justifyContent: "space-around",
            alignItems: "center",
          }}
        >
          {orderTest === 4 ? (
            <View
              style={{
                display: "flex",
                flex: 1,
                justifyContent: "space-around",
                alignItems: "center",
              }}
            >
              <Text
                style={{
                  lineHeight: 32,
                  fontSize: 24,
                  textAlign: "center",
                  fontWeight: "500",
                  color: "#594f45",
                }}
              >
                You have finished all parts of Gender-Career Bias Test!
              </Text>
              <Link
                to={`/testIAT/result/${encodeURIComponent(
                  JSON.stringify(testResult)
                )}`}
                style={{ textDecoration: "none" }}
              >
                <TouchableOpacity
                  style={{
                    backgroundColor: "#132b4f",
                    marginVertical: 12,
                    borderRadius: 100,
                    padding: 12,
                    minWidth: 160,
                  }}
                >
                  <Text
                    style={{
                      color: "#fdfcfc",
                      fontWeight: "500",
                      textAlign: "center",
                    }}
                  >
                    Finish test
                  </Text>
                </TouchableOpacity>
              </Link>
            </View>
          ) : (
            <View
              style={{
                display: "flex",
                flex: 1,
                justifyContent: "space-around",
                alignItems: "center",
              }}
            >
              <Text
                style={{
                  lineHeight: 32,
                  fontSize: 24,
                  textAlign: "center",
                  fontWeight: "500",
                  color: "#594f45",
                }}
              >
                You have finished Part {orderTest} of Gender Career Bias Test.
                Please pay attention to the buttons' value on the next part.
              </Text>
              <TouchableOpacity
                style={{
                  backgroundColor: "#132b4f",
                  marginVertical: 12,
                  borderRadius: 100,
                  padding: 12,
                  minWidth: 160,
                }}
                onPress={continueNextTest}
              >
                <Text
                  style={{
                    color: "#fdfcfc",
                    fontWeight: "500",
                    textAlign: "center",
                  }}
                >
                  Continue next part
                </Text>
              </TouchableOpacity>
            </View>
          )}
        </View>
      )}

      <BottomNavBar />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column",
    height: "100vh",
    width: "100vw",
    paddingHorizontal: 24,
    paddingVertical: 72,
    alignItems: "center",
    //justifyContent: "space-around",
    backgroundColor: "#fdfcfc",
  },
  title: {
    fontSize: 24,
    fontWeight: "bold",
    color: "#594f45",
    textAlign: "center",
    // marginBottom: 8,
  },
  heading: {
    fontSize: 16,
    color: "#594f45",
  },
  buttonMain: {
    backgroundColor: "#132b4f",
    borderRadius: 100,
    padding: 12,
    paddingHorizontal: 24,
    minWidth: 160,
    marginHorizontal: 4,
  },
  buttonAlt: {
    backgroundColor: "#fdfcfc",
    borderColor: "#132b4f",
    borderWidth: 1,
    borderRadius: 100,
    padding: 12,
    paddingHorizontal: 24,
    minWidth: 160,
    marginHorizontal: 4,
  },
});

export default IATtest_Main;
