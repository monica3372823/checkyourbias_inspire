import { View, Text, TouchableOpacity, StyleSheet } from "react-native";
import { useNavigation } from "@react-navigation/native";
import { NativeStackNavigationProp } from "@react-navigation/native-stack";
import { Divider } from "@mui/material";
import { SearchOutlined } from "@mui/icons-material";

function RecordSection({
  dateTest,
  resultTest,
}: {
  dateTest: string;
  resultTest: string;
}) {
  return (
    <View
      style={{
        display: "flex",
        flexDirection: "row",
        width: "100vw",
        paddingHorizontal: 24,
        paddingVertical: 8,
        justifyContent: "center",
        alignItems: "center",
      }}
    >
      <View
        style={{
          borderWidth: 1 / 4,
          borderRadius: 50,
          padding: 12,
          margin: 8,
          display: "flex",
          flex: 1,
        }}
      >
        <Text>
          {dateTest} <Text style={{ fontWeight: "300" }}>{resultTest}</Text>
        </Text>
      </View>
      <SearchOutlined />
    </View>
  );
}

function IATtest_Record() {
  // dummy test result
  const ListTestRecord = {
    "2022/06/09": "Slightly bias in general",
    "2021/06/09": "Slightly bias in general",
    "2020/06/09": "Slightly bias in general",
    "2019/06/09": "Slightly bias in general",
  };

  const ArrayTestRecord = Object.entries(ListTestRecord);

  return (
    <View style={styles.container}>
      <View
        style={{
          display: "flex",
          justifyContent: "center",
          width: "100vw",
          paddingHorizontal: 48,
        }}
      >
        <Text style={styles.title}>Record</Text>
        <Divider style={{ marginTop: 8, marginBottom: 8 }} />
        <Text style={{ textAlign: "center" }}>Potential Biases Test</Text>
      </View>

      <View>
        {ArrayTestRecord.map((item, index) => {
          return <RecordSection dateTest={item[0]} resultTest={item[1]} />;
        })}
        ;
      </View>

      <TouchableOpacity
        style={styles.buttonMain}
        // onPress={() => navigation.goBack()}
      >
        <Text style={{ color: "#fdfcfc", fontWeight: "500" }}>
          Close Record
        </Text>
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column",
    height: "100vh",
    width: "100vw",
    padding: 24,
    alignItems: "center",
    justifyContent: "space-around",
    backgroundColor: "#fdfcfc",
  },
  title: {
    fontSize: 24,
    fontWeight: "bold",
    color: "#594f45",
    textAlign: "center",
    // marginBottom: 8,
  },
  heading: {
    fontSize: 16,
    color: "#594f45",
  },
  buttonMain: {
    backgroundColor: "#132b4f",
    borderRadius: 100,
    padding: 12,
    paddingHorizontal: 36,
  },
  buttonAlt: {
    backgroundColor: "#fdfcfc",
    borderColor: "#132b4f",
    borderWidth: 1,
    borderRadius: 100,
    padding: 12,
    paddingHorizontal: 36,
  },
});

export default IATtest_Record;
