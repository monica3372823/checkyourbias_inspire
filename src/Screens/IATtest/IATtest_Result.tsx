import { View, Text, TouchableOpacity, StyleSheet } from "react-native";
import { useEffect, useState } from "react";
import { useNavigation } from "@react-navigation/native";
import { NativeStackNavigationProp } from "@react-navigation/native-stack";
import { Divider, Alert, TextareaAutosize } from "@mui/material";
import { Link, useParams } from "react-router-dom";
import BottomNavBar from "../../BottomNav";

enum ResultType {
  "No Bias",
  "Slightly Bias",
  "Moderately Bias",
  "Hardly Bias",
}

function ResultDesc({
  TestType,
  TestResult,
  ResultDesc,
}: {
  TestType: string;
  TestResult: ResultType;
  ResultDesc: string;
}) {
  const ColorResult = ["#15803D", "#EAB308", "#EA580C", "#E11D48"];
  const TextResult = [
    "No Bias",
    "Slightly Bias",
    "Moderately Bias",
    "Hardly Bias",
  ];

  return (
    <View
      style={{
        width: "100vw",
        paddingHorizontal: 24,
        paddingVertical: 8,
        marginVertical: 36,
      }}
    >
      <Text
        style={{
          fontWeight: "500",
          color: "#594f45",
          textAlign: "center",
          fontSize: 20,
        }}
      >
        Result:{" "}
        <Text style={{ color: ColorResult[TestResult], fontWeight: "bold" }}>
          {TextResult[TestResult]}
        </Text>
      </Text>

      <View
        style={{
          borderWidth: 1 / 4,
          borderRadius: 8,
          padding: 12,
          alignItems: "center",
          marginVertical: 8,
        }}
      >
        <Text
          style={{
            fontSize: 12,
            color: "#594f45",
            textAlign: "center",
          }}
        >
          {ResultDesc}
        </Text>
      </View>

      <View
        style={{
          padding: 12,
          alignItems: "center",
          marginVertical: 16,
        }}
      >
        <Text
          style={{
            fontWeight: "500",
            color: "#594f45",
            textAlign: "center",
            //marginBottom: 6,
          }}
        >
          See other peoples' result of this test
        </Text>
        <img
          src="/resultComparison.png"
          alt="Result comparison for Gender-Career Bias Test"
          style={{ height: 180 }}
        />
      </View>
    </View>
  );
}

function IATtest_Result() {
  const ListTestName = ["Gender-Career", "Skin tone", "Age", "Sexuality"];
  //const { numMistake } = useParams();
  const { testResultParam } = useParams();
  const [testResult, setTestResult] = useState({
    totalTime3: 0,
    totalTime4: 0,
    totalMistakes: 0,
  });

  useEffect(() => {
    if (testResultParam) {
      const decodedString = decodeURIComponent(testResultParam);
      const testResultObject = JSON.parse(decodedString);
      setTestResult(testResultObject);
    }
  }, [testResultParam]);
  //const description = `You made ${numMistake} potentially bias answers. You have a slight tendency to associate one gender with career and the other with family`;

  const description =
    testResult.totalTime3 <= testResult.totalTime4
      ? `You took longer time to associate female with career and male with family 
    rather than the opposite. You also made ${testResult.totalMistakes} potentially biased associations.\n\nYou might have an implicit preference of male with career and female with family.`
      : `You took longer time to associate  male with career and female with family 
    and ${testResult.totalTime4} in total to associate male with family and female with career rather than the opposite.You also made ${testResult.totalMistakes}
    potentially biased associations.\n\nYou might have an implicit preference of female with career and male with family.`;

  return (
    <View style={styles.container}>
      <View
        style={{
          display: "flex",
          justifyContent: "center",
          width: "100vw",
          paddingHorizontal: 48,
          marginBottom: 36,
        }}
      >
        <Text style={styles.title}>Result</Text>
        <Divider style={{ marginTop: 8, marginBottom: 8 }} />
        <Text style={{ textAlign: "center" }}>Gender-Career Bias Test</Text>
      </View>

      <Alert severity="warning">
        This is just one way to know your potential biases. To learn more, click{" "}
        <strong>here</strong>
      </Alert>

      <View>
        {/* {ListTestName.map((item, index) => {
          return (
            <ResultDesc
              key={index}
              TestType={item}
              TestResult={ListTestResult[index]}
            />
          );
        })}
        ; */}
        <ResultDesc
          TestType="Gender-Career Bias Test"
          TestResult={1}
          ResultDesc={description}
        />
      </View>

      <View
        style={{
          display: "flex",
          flexDirection: "row",
          justifyContent: "space-around",
          width: "100vw",
          paddingHorizontal: 24,
        }}
      >
        <TouchableOpacity
          style={styles.buttonMain}
          // onPress={() => navigation.goBack()}
        >
          <Text
            style={{
              color: "#fdfcfc",
              fontWeight: "500",
              textAlign: "center",
            }}
          >
            Continue next test
          </Text>
        </TouchableOpacity>

        <Link to="/home" style={{ textDecoration: "none" }}>
          <TouchableOpacity
            style={styles.buttonAlt}
            // onPress={() => navigation.goBack()}
          >
            <Text
              style={{
                color: "#132b4f",
                fontWeight: "300",
                textAlign: "center",
              }}
            >
              Continue test later{" "}
            </Text>
          </TouchableOpacity>
        </Link>

        <BottomNavBar />
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column",
    height: "100vh",
    width: "100vw",
    paddingVertical: 48,
    paddingHorizontal: 24,
    alignItems: "center",
    //justifyContent: "space-between",
    backgroundColor: "#fdfcfc",
  },
  title: {
    fontSize: 24,
    fontWeight: "bold",
    color: "#594f45",
    textAlign: "center",
    // marginBottom: 8,
  },
  heading: {
    fontSize: 16,
    color: "#594f45",
  },
  buttonMain: {
    backgroundColor: "#132b4f",
    borderRadius: 100,
    padding: 12,
    minWidth: 140,
  },
  buttonAlt: {
    backgroundColor: "#fdfcfc",
    borderColor: "#132b4f",
    borderWidth: 1,
    borderRadius: 100,
    padding: 12,
    minWidth: 140,
  },
});

export default IATtest_Result;
