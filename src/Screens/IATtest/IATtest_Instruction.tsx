import { View, Text, TouchableOpacity, StyleSheet } from "react-native";
import { Divider, Stack } from "@mui/material";
import { Link } from "react-router-dom";
import BottomNavBar from "../../BottomNav";

function IATtest_Instruction() {
  return (
    <View style={styles.container}>
      <View
        style={{
          display: "flex",
          justifyContent: "center",
          width: "100vw",
          paddingHorizontal: 48,
        }}
      >
        <Text style={styles.title}>Gender-Career Bias Test</Text>
        <Divider style={{ marginTop: 8, marginBottom: 8 }} />
        <Text style={{ textAlign: "center" }}>
          Potential Biases Test inspired by Implicit Association Test
        </Text>
      </View>

      <Text
        style={{
          alignItems: "center",
          lineHeight: 24,
          fontSize: 16,
          textAlign: "center",
        }}
      >
        In this test, you will need to categorize a word into group as fast as
        you can. You will see two buttons that correspond to your answer, so
        choose one for each word.
      </Text>

      <Stack spacing={1}>
        <View
          style={{
            borderWidth: 1 / 4,
            borderColor: "#594f45",
            padding: 12,
            width: "100%",
            marginBottom: 4,
            display: "flex",
            flexDirection: "row",
            borderRadius: 4,
          }}
        >
          <View style={{ minWidth: 60 }}>
            <Text style={{ fontWeight: "500", borderColor: "#594f45" }}>
              Male:
            </Text>
          </View>
          <Text>Ben, Paul, Daniel, John, Jeffrey</Text>
        </View>
        <View
          style={{
            borderWidth: 1 / 4,
            borderColor: "#594f45",
            padding: 12,
            width: "100%",
            marginBottom: 4,
            display: "flex",
            flexDirection: "row",
            borderRadius: 4,
          }}
        >
          <View style={{ minWidth: 60 }}>
            <Text style={{ fontWeight: "500", borderColor: "#594f45" }}>
              Female:
            </Text>
          </View>
          <Text>Rebecca, Michelle, Emily, Julia, Anna</Text>
        </View>
        <View
          style={{
            borderWidth: 1 / 4,
            borderColor: "#594f45",
            padding: 12,
            width: "100%",
            marginBottom: 4,
            display: "flex",
            flexDirection: "row",
            borderRadius: 4,
          }}
        >
          <View style={{ minWidth: 60 }}>
            <Text style={{ fontWeight: "500", borderColor: "#594f45" }}>
              Career:
            </Text>
          </View>
          <Text>
            Career, Corporation, Salary, Office, Professional, Management,
            Business
          </Text>
        </View>
        <View
          style={{
            borderWidth: 1 / 4,
            borderColor: "#594f45",
            padding: 12,
            width: "100%",
            marginBottom: 4,
            display: "flex",
            flexDirection: "row",
            borderRadius: 4,
          }}
        >
          <View style={{ minWidth: 60 }}>
            <Text style={{ fontWeight: "500", borderColor: "#594f45" }}>
              Family:
            </Text>
          </View>
          <Text>
            Wedding, Marriage, Parents, Relatives, Family, Home, Children
          </Text>
        </View>
      </Stack>

      <Text
        style={{
          alignItems: "center",
          lineHeight: 24,
          fontSize: 16,
          textAlign: "center",
        }}
      >
        The first two parts help you to familiarize yourself with the words
        association while the last two parts will test your potential biases.
      </Text>

      <View
        style={{
          display: "flex",
          flexDirection: "row",
          justifyContent: "space-evenly",
          width: "100vw",
          padding: 24,
          marginBottom: 24,
        }}
      >
        <Link to="/testIAT/main" style={{ textDecoration: "none" }}>
          <TouchableOpacity
            style={styles.buttonMain}
            // onPress={() => navigation.goBack()}
          >
            <Text
              style={{
                color: "#fdfcfc",
                fontWeight: "500",
                textAlign: "center",
              }}
            >
              Start test now
            </Text>
          </TouchableOpacity>
        </Link>

        <Link to="/home" style={{ textDecoration: "none" }}>
          <TouchableOpacity
            style={styles.buttonAlt}
            // onPress={() => navigation.goBack()}
          >
            <Text
              style={{
                color: "#132b4f",
                fontWeight: "300",
                textAlign: "center",
              }}
            >
              Cancel
            </Text>
          </TouchableOpacity>
        </Link>
      </View>

      <BottomNavBar />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column",
    height: "100vh",
    width: "100vw",
    padding: 24,
    alignItems: "center",
    justifyContent: "space-around",
    backgroundColor: "#fdfcfc",
  },
  title: {
    fontSize: 24,
    fontWeight: "bold",
    color: "#594f45",
    textAlign: "center",
    // marginBottom: 8,
  },
  heading: {
    fontSize: 16,
    color: "#594f45",
  },
  buttonMain: {
    backgroundColor: "#132b4f",
    borderRadius: 100,
    padding: 12,
    minWidth: 140,
  },
  buttonAlt: {
    backgroundColor: "#fdfcfc",
    borderColor: "#132b4f",
    borderWidth: 1,
    borderRadius: 100,
    padding: 12,
    minWidth: 140,
  },
});

export default IATtest_Instruction;
