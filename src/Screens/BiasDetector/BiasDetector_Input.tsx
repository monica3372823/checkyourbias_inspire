import React, { useState, useEffect } from "react";
import {
  Text,
  View,
  TouchableOpacity,
  StyleSheet,
  TextInput,
} from "react-native";
import { Divider, LinearProgress, Input, Alert } from "@mui/material";
import { Padding, WarningOutlined } from "@mui/icons-material";
import { Link } from "react-router-dom";
import BottomNavBar from "../../BottomNav";

interface BiasResult {
  sentence: string;
  result: string;
  score: number;
}

function BiasClassify({ result, score }: BiasResult) {
  return (
    <View
      style={{
        width: "100%",
        paddingVertical: 8,
        marginVertical: 12,
      }}
    >
      <Text style={{ fontWeight: "500", fontSize: 20, textAlign: "center" }}>
        Sentence is classified as: {result}
      </Text>
      <LinearProgress
        style={{
          marginTop: 16,
          marginBottom: 16,
          height: 8,
          borderRadius: 4,
        }}
        variant="determinate"
        value={score}
      />
      {result == "Biased" && (
        <Text style={{ textAlign: "center" }}>
          Your sentence might contain some words that indicate bias.
        </Text>
      )}
    </View>
  );
}

const urlAPI =
  "https://8c285qzfjyx-496ff2e9c6d22116-5000-colab.googleusercontent.com/";

function BiasDetector_Input() {
  const [sentenceInput, setSentenceInput] = useState<string>("");
  const [checkSentence, setCheckSentence] = useState(false);
  const [biasResult, setBiasResult] = useState<BiasResult>({
    sentence: "",
    result: "",
    score: 0,
  });

  const biasSentenceExample: BiasResult[] = [
    //"You have excellent attention to detail, but you can be a bit emotional at times.":
    {
      sentence:
        "You have excellent attention to detail, but you can be a bit emotional sometimes.",
      result: "Non-biased",
      score: 38.8,
    },
    //"You have excellent attention to detail, but like other girls that I know, I noticed that you can be a bit emotional at times.":
    {
      sentence:
        "You have excellent attention to detail, but like other girls that I know, you can be a bit emotional sometimes.",
      result: "Biased",
      score: 76.5,
    },
  ];

  function handleCheckSentence() {
    console.log(sentenceInput);

    for (let i = 0; i < biasSentenceExample.length; i++) {
      if (biasSentenceExample[i].sentence == sentenceInput) {
        setBiasResult({
          sentence: biasSentenceExample[i].sentence,
          result: biasSentenceExample[i].result,
          score: biasSentenceExample[i].score,
        });
      }
    }
  }

  // useEffect(() => {
  //   if (checkSentence) {
  //     fetch(urlAPI + "/classifier/" + encodeURIComponent(sentenceInput))
  //       .then((response) => response.json())
  //       .then((result) => {
  //         setBiasResult({
  //           result: result.label as string,
  //           score: result.score as number,
  //         });
  //       });
  //     // .catch((error) => {
  //     //   console.error(error);
  //     // });
  //   }
  // }, [checkSentence]);

  return (
    <View style={styles.container}>
      <View
        style={{
          display: "flex",
          justifyContent: "center",
          width: "100vw",
          paddingHorizontal: 48,
        }}
      >
        <Text style={styles.title}>In-Text Bias Checker</Text>
        <Divider style={{ marginTop: 8, marginBottom: 8 }} />
      </View>

      <Alert severity="warning" sx={{ marginY: 2 }}>
        This feature only do predection and might not be accurate. To learn more
        about potential biases in sentences, click <strong>here</strong>{" "}
      </Alert>

      <View
        style={{
          borderRadius: 10,
          width: "100%",
          padding: 12,
          display: "flex",
          //alignContent: "flex-start",
          minHeight: 160,
          borderWidth: 1 / 4,
          marginBottom: 24,
          marginTop: 12,
        }}
      >
        <Input
          placeholder="Input your text here"
          multiline
          disableUnderline
          fullWidth
          //placeholderTextColor={"#A3A3A3"}
          value={sentenceInput}
          onChange={(event) => setSentenceInput(event.target.value)}
        />
        {/* <Text style={{ fontWeight: "300", color: "#A3A3A3" }}>
            Input your text here
          </Text>
        </TextInput> */}
      </View>

      <TouchableOpacity
        style={styles.buttonMain}
        onPress={() => {
          handleCheckSentence();
          setCheckSentence(true);
        }}
      >
        <Text style={{ color: "#fdfcfc", fontWeight: "500" }}>Check Bias</Text>
      </TouchableOpacity>

      {checkSentence && (
        <View
          style={{
            width: "100vw",
            paddingHorizontal: 48,
            marginVertical: 24,
          }}
        >
          {/* <Text style={{ fontSize: 20, fontWeight: "500", color: "bold" }}>
            Result
          </Text> */}
          <View>
            <BiasClassify
              sentence={sentenceInput}
              result={biasResult.result}
              score={biasResult.score}
            />
          </View>
        </View>
      )}

      <BottomNavBar />

      {/* <Link to="/biasDetector/result" style={{ textDecoration: "none" }}>
        <TouchableOpacity
          style={styles.buttonMain}
          // onPress={() => navigation.goBack()}
        >
          <Text style={{ color: "#fdfcfc", fontWeight: "500" }}>
            See Suggestion
          </Text>
        </TouchableOpacity>
      </Link> */}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column",
    height: "100vh",
    width: "100vw",
    paddingHorizontal: 24,
    paddingVertical: 48,
    alignItems: "center",
    //justifyContent: "space-evenly",
    backgroundColor: "#fdfcfc",
  },
  title: {
    fontSize: 24,
    fontWeight: "bold",
    color: "#594f45",
    textAlign: "center",
    // marginBottom: 8,
  },
  heading: {
    fontSize: 16,
    color: "#594f45",
  },
  buttonMain: {
    backgroundColor: "#132b4f",
    borderRadius: 100,
    padding: 12,
    paddingHorizontal: 36,
  },
  buttonAlt: {
    backgroundColor: "#fdfcfc",
    borderColor: "#132b4f",
    borderWidth: 1,
    borderRadius: 100,
    padding: 12,
    paddingHorizontal: 36,
  },
});

export default BiasDetector_Input;
