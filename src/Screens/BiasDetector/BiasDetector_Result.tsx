import React from "react";
import {
  Text,
  View,
  TouchableOpacity,
  StyleSheet,
  TextInput,
} from "react-native";
import { Divider, LinearProgress } from "@mui/material";
import { Padding, InfoOutlined } from "@mui/icons-material";
import { Link } from "react-router-dom";

function SuggestionSection({ fixSuggestion }: { fixSuggestion: string }) {
  return (
    <View
      style={{
        width: "full",
        paddingVertical: 4,
      }}
    >
      <View
        style={{
          display: "flex",
          flexDirection: "row",
          borderWidth: 1 / 4,
          borderRadius: 100,
          padding: 12,
          alignItems: "center",
          marginBottom: 8,
        }}
      >
        <InfoOutlined color="primary" />
        <Text style={{ marginLeft: 8 }}>{fixSuggestion}</Text>
      </View>
    </View>
  );
}

function BiasDetector_Result() {
  const resultSuggestion = [
    "the Doctor. She says....",
    "the Doctor. She says....",
    "the Doctor. She says....",
    "the Doctor. She says....",
  ];

  return (
    <View style={styles.container}>
      <View
        style={{
          display: "flex",
          justifyContent: "center",
          width: "100vw",
          paddingHorizontal: 48,
        }}
      >
        <Text style={styles.title}>In-Text Bias Editor</Text>
        <Divider style={{ marginTop: 8, marginBottom: 8 }} />
      </View>

      <View
        style={{
          borderRadius: 10,
          width: "90%",
          padding: 12,
          height: 160,
          borderWidth: 1 / 4,
          marginVertical: 24,
        }}
      >
        <TextInput
          placeholder="Input your text here"
          multiline
          placeholderTextColor={"#A3A3A3"}
        />
        {/* <Text style={{ fontWeight: "300", color: "#A3A3A3" }}>
            Input your text here
          </Text>
        </TextInput> */}
      </View>

      <View
        style={{
          width: "100vw",
          paddingHorizontal: 48,
          marginBottom: 24,
        }}
      >
        <Text
          style={{
            fontSize: 20,
            fontWeight: "500",
            color: "bold",
            marginBottom: 8,
          }}
        >
          Suggestions
        </Text>
        <View>
          {resultSuggestion.map((item, index) => (
            <SuggestionSection key={index} fixSuggestion={item} />
          ))}
        </View>
      </View>

      <Link to="/biasDetector/input" style={{ textDecoration: "none" }}>
        <TouchableOpacity
          style={styles.buttonMain}
          // onPress={() => navigation.goBack()}
        >
          <Text style={{ color: "#fdfcfc", fontWeight: "500" }}>
            Back to Result
          </Text>
        </TouchableOpacity>
      </Link>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column",
    height: "100vh",
    width: "100vw",
    padding: 24,
    alignItems: "center",
    //justifyContent: "space-evenly",
    backgroundColor: "#fdfcfc",
  },
  title: {
    fontSize: 24,
    fontWeight: "bold",
    color: "#594f45",
    textAlign: "center",
    // marginBottom: 8,
  },
  heading: {
    fontSize: 16,
    color: "#594f45",
  },
  buttonMain: {
    backgroundColor: "#132b4f",
    borderRadius: 100,
    padding: 12,
    paddingHorizontal: 36,
  },
  buttonAlt: {
    backgroundColor: "#fdfcfc",
    borderColor: "#132b4f",
    borderWidth: 1,
    borderRadius: 100,
    padding: 12,
    paddingHorizontal: 36,
  },
});

export default BiasDetector_Result;
