import React from "react";
import {
  Text,
  View,
  TouchableOpacity,
  StyleSheet,
  Linking,
} from "react-native";
import { Divider, Avatar, Stack } from "@mui/material";
import { Padding, SearchOutlined } from "@mui/icons-material";
import { Link } from "react-router-dom";
import BottomNavBar from "../BottomNav";

function MenuOption({
  colorCode,
  menuText,
  imageSource,
}: {
  colorCode: string;
  menuText: string;
  imageSource: string;
}) {
  return (
    <View
      style={{
        padding: 12,
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        maxWidth: 120,
      }}
    >
      <TouchableOpacity
        style={{
          backgroundColor: colorCode,
          borderRadius: 96,
          height: 96,
          width: 96,
          marginBottom: 8,
          //alignItems: "baseline",
        }}
      >
        <Avatar
          src={imageSource}
          sx={{
            width: "60%",
            height: "60%",
            alignSelf: "center",
            paddingY: 2,
          }}
        />
      </TouchableOpacity>
      <Text style={{ fontWeight: "500", textAlign: "center" }}>{menuText}</Text>
    </View>
  );
}

function HomeMenu() {
  const username = localStorage.getItem("userName");

  return (
    <View style={styles.container}>
      <Stack spacing={3}>
        <View
          style={{
            display: "flex",
            alignItems: "flex-start",
            paddingHorizontal: 24,
            width: "100vw",
          }}
        >
          <Text style={styles.title}>Hi, {username}</Text>
          <Text style={{ marginVertical: 8 }}>
            Let's learn more about unconscious biases today!
          </Text>
          <Divider flexItem />
          <View
            style={{
              borderWidth: 1 / 4,
              borderRadius: 50,
              padding: 8,
              display: "flex",
              flex: 1,
              flexDirection: "row",
              alignItems: "center",
              width: "100%",
              marginVertical: 16,
            }}
          >
            <SearchOutlined />
            <Text style={{ fontSize: 16, fontWeight: "200" }}>Search here</Text>
          </View>
        </View>

        <View
          style={{
            display: "flex",
            alignItems: "flex-start",
            paddingHorizontal: 24,
            width: "100vw",
          }}
        >
          <Text style={{ fontSize: 20, fontWeight: "700", color: "#594f45" }}>
            Article of the Day
          </Text>
          <View
            style={{
              borderRadius: 10,
              backgroundColor: "#E0F2FE",
              marginTop: 8,
              width: "100%",
              padding: 12,
            }}
          >
            <Text
              style={{
                fontSize: 16,
                fontWeight: "700",
                marginBottom: 4,
                color: "#594f45",
              }}
            >
              Combating Unconscious Biases in Medicine in 2023
            </Text>
            <Text style={{ fontSize: 12, color: "#594f45" }}>
              Unconscious biases continue to hold back women in medicine, but
              research shows how to fight them and get closer to true equity and
              inclusion
            </Text>
            <TouchableOpacity
              style={styles.buttonMain}
              onPress={() =>
                Linking.openURL(
                  "https://theconversation.com/unconscious-biases-continue-to-hold-back-women-in-medicine-but-research-shows-how-to-fight-them-and-get-closer-to-true-equity-and-inclusion-200496"
                )
              }
            >
              <Text
                style={{
                  color: "#fdfcfc",
                  fontWeight: "500",
                  textAlign: "center",
                }}
              >
                See full article
              </Text>
            </TouchableOpacity>
          </View>
        </View>

        <View
          style={{
            display: "flex",
            paddingHorizontal: 24,
            flexDirection: "row",
            flexWrap: "wrap",
            justifyContent: "space-around",
          }}
        >
          <Link to="/testIAT/introduction" style={{ textDecoration: "none" }}>
            <MenuOption
              colorCode="#7dd3fc"
              menuText="Potential Biases Test"
              imageSource="/biasTest.png"
            />
          </Link>

          <Link to="/biasDetector/input" style={{ textDecoration: "none" }}>
            <MenuOption
              colorCode="#0ea5e9"
              menuText="In-Text Bias Checker"
              imageSource="/biasEditor.png"
            />
          </Link>

          <Link to="/learningSpace" style={{ textDecoration: "none" }}>
            <MenuOption
              colorCode="#0284c7"
              menuText="Learning Space"
              imageSource="/book.png"
            />
          </Link>

          <Link to="/discussionHub" style={{ textDecoration: "none" }}>
            <MenuOption
              colorCode="#0369a1"
              menuText="Discussion Hub"
              imageSource="/learningHub.png"
            />
          </Link>
        </View>
      </Stack>
      <BottomNavBar />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column",
    height: "100vh",
    width: "100vw",
    padding: 48,
    alignItems: "center",
    //justifyContent: "space-evenly",
    backgroundColor: "#fdfcfc",
  },
  title: {
    fontSize: 24,
    fontWeight: "bold",
    color: "#594f45",
    textAlign: "center",
    // marginBottom: 8,
  },
  heading: {
    fontSize: 16,
    color: "#594f45",
  },
  buttonMain: {
    backgroundColor: "#132b4f",
    borderRadius: 100,
    padding: 12,
    maxWidth: 120,
    marginTop: 12,
  },
  buttonAlt: {
    backgroundColor: "#fdfcfc",
    borderColor: "#132b4f",
    borderWidth: 1,
    borderRadius: 100,
    padding: 12,
    paddingHorizontal: 36,
  },
});

export default HomeMenu;
