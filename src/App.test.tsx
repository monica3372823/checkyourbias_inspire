import React from "react";
import { render, screen } from "@testing-library/react";
import App from "./App";
import LandingScreen from "./Screens/Landing";

test("renders learn react link", () => {
  render(<LandingScreen />);
  const linkElement = screen.getByText(/learn react/i);
  expect(linkElement).toBeInTheDocument();
});
